# HAGPoddermedia

## Description

My attempt to develop Home Assistant Media provider integration for Home Assistant.

## Contributing

Free to contribute, free to use any code I have made here. I just want a podcast media provider entity thingie for Home Assistant. Didn't see one and thus thought.... like how hard can it be...... I guess I shall see

## Authors and acknowledgment
Thanks to the previously existing https://github.com/custom-components/gpodder custom component and https://github.com/iantrich/podcast-card for a starting place. 

## License
MIT License

## Project status
Work in progress
