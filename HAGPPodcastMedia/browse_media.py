"""Support for media browsing."""
import asyncio
import contextlib
import logging

from homeassistant.components.media_source.models import (
    BrowseMediaSource,
    MediaSource,
    MediaSourceItem,
    PlayMedia,
)
from homeassistant.components.media_player import (
    BrowseError,
    BrowseMedia,
    MediaClass,
    MediaType,
)

PLAYABLE_MEDIA_TYPES = [
    MediaType.PODCAST,
    MediaType.EPISODE,
]

CONTAINER_TYPES_SPECIFIC_MEDIA_CLASS = {
    MediaType.PODCAST: MediaClass.PODCAST,
}

CHILD_TYPE_MEDIA_CLASS = {
    MediaType.SEASON: MediaClass.SEASON,
    MediaType.EPISODE: MediaClass.EPISODE,
}

_LOGGER = logging.getLogger(__name__)


class UnknownMediaType(BrowseError):
    """Unknown media type."""


async def async_get_media_source(hass: HomeAssistant) -> GPPodcastMediaSource:
    """Set up podcast media source."""
    entry = hass.config_entries.async_entries(DOMAIN)[0]
    return GPPodcastMediaSource(hass, entry)


class GPPodcastMediaSource(MediaSource):
    pass